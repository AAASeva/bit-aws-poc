#!/bin/bash

#
# Please refer to the installation instruction on the docker page
# https://docs.docker.com/engine/install/ubuntu/
#

SCRIPT=$(basename $0)

echo "#"
echo "# $SCRIPT - Install docker engine"
echo "#"

DOCKER_GPG=https://download.docker.com/linux/ubuntu/gpg
DOCKER_URL=https://download.docker.com/linux/ubuntu 

if [ "$(id -u)" != "0" ]; then
    echo "$SCRIPT - Submitted by user $(whoami)" 1>&2
    echo "$SCRIPT - This script must be run from the root account. Please use \$ sudo $SCRIPT" 1>&2
    echo "$SCRIPT - Executing sudo $0 $@" 1>&2
    exec sudo "$0" "$@"
    exit 1
fi

apt-get update
apt-get install apt-transport-https ca-certificates curl gnupg-agent software-properties-common
curl -fsSL $DOCKER_GPG | apt-key add -
apt-key fingerprint 0EBFCD88
add-apt-repository    "deb [arch=amd64] $DOCKER_URL $(lsb_release -cs) stable"
apt-get update
apt-get install docker-ce docker-ce-cli containerd.io
docker run hello-world
